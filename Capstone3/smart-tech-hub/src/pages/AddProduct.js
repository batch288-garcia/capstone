import {Container, Row, Col, Form, Button } from "react-bootstrap";

import {useState, useEffect} from 'react';

import Swal from "sweetalert2";
import { useNavigate} from "react-router-dom";

export default function AddProduct(){

    const [name, setName ] = useState('');
    const [description, setDescription ] = useState('');
    const [price, setPrice ]= useState('');
    const [isActive, setIsActive ]= useState('');

    const [isDisabled, setIisDisabled] = useState(true);
    const [imgUrl, setImgUrl] = useState('');
    
  const navigate = useNavigate();

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '' )
        {
            setIisDisabled(false)
        }else{
            setIisDisabled(true)
        }
    }, [name, price, description]);


    useEffect(() => {
            setImgUrl("https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg");
    }, [])

    function addProduct(event){
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method : 'POST',
            headers : {
                Authorization : `Bearer ${localStorage.getItem('token')}`,
                'Content-Type' : 'Application/json'
            },
            body: JSON.stringify({
                imgUrl: imgUrl,
                name: name,
                description: description,
                price: price,
                isActive: isActive.toLowerCase()
            })

        })
        .then((result) => result.json())
        .then((data) => {
            console.log(data)
          if (data)  {
            Swal.fire({
              title: `Product Successfully Added `,
              icon: "success",
              text: `${name} is added to store`,
            });
  
            navigate("/allProduct");
          } else {
            Swal.fire({
              title: `Failed to add ${name}`,
              icon: "error",
              text: `${name} is already in store`,
            });
          }
        });

    }


  return(
    <Container className="mt-3 ">
        <Row>
            <Col>
            <h1 className=" mb-3 title-text text-center">Add Product</h1>
            <Form onSubmit = {event => addProduct(event)}>

            <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Image URL</Form.Label>
                    <Form.Control 
                                    type="text"
                                    value = {imgUrl}
                                    onChange = {event => setImgUrl(event.target.value)}
                                    placeholder="Img URL" />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Status</Form.Label>
                    <Form.Control 
                        type = "Text"
                        value = {isActive}
                        onChange = {event => setIsActive(event.target.value)}
                        placeholder = "Status" 
                    />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control 
                        type = "number"
                        value = {price}
                        onChange = {event => setPrice(event.target.value)}
                        placeholder = "0.00" 
                    />

                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Name</Form.Label>
                    <Form.Control 
                                    type="text"
                                    value = {name}
                                    onChange = {event => setName(event.target.value)}
                                    placeholder="Product Name" />
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Description</Form.Label>
                    <Form.Control 
                                type="text" 
                                value = {description} 
                                onChange = {event => setDescription(event.target.value)}
                                placeholder="Product Description" />
                </Form.Group>

                <div className="text-center mt-2">
                    <Button variant="primary" 
                        disabled = {isDisabled}
                        type="submit">
                Add Product</Button>
                    </div>
                </Form>
            </Col>
        </Row>
    </Container>
  )
}