import { Container, Row, Col, Button, Form } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import { useEffect, useState, useContext} from "react";

import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Register() {

  const { user, setUser } = useContext(UserContext);

  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [isDisabled, setIsDisbled] = useState(true);

  const navigate = useNavigate();

  useEffect(() => {
    if (userName !== "" && email !== "" && password !== "") {
      setIsDisbled(false);
    } else {
      setIsDisbled(true);
    }
  }, [userName, email, password]);

  function register(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
      method: "POST",
      headers: {
        "Content-type": "Application/json",
      },
      body: JSON.stringify({
        userName: userName,
        email: email,
        password: password,
      }),
    })
      .then((Response) => Response.json())
      .then((data) => {
        if (data) {
          Swal.fire({
            title: `You have successfully registered as ${userName}`,
            icon: "success",
            text: "Please use another email.",
          });

          navigate("/login");
        } else {
          Swal.fire({
            title: "Email or UserName is already registered",
            icon: "error",
            text: "Please use another email.",
          });
        }
      });
  }

  return (
    user.id === null ?
    <Container className="page-bg">
      <Row>
        <Col className="col-sm-9 col-md-8  col-lg-6 mx-auto">
          <h1 className=" mar-top mb-3 title-text text-center">Register</h1>
          <Form
            className="mt-3 decor p-3"
            onSubmit={(event) => register(event)}
          >
            <Form.Group className="mb-3" controlId="fromBasicUserName">
              <Form.Label className="bolder-text">Username</Form.Label>
              <Form.Control
                type="text"
                value={userName}
                onChange={(event) => setUserName(event.target.value)}
                placeholder="Username"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label className="bolder-text">Email address</Form.Label>
              <Form.Control
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                placeholder="sample@mail.com"
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label className="bolder-text">Password</Form.Label>
              <Form.Control
                type="password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                placeholder="Password"
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <p>
                Have an account? <Link to={"/login"}>Log in here</Link>
              </p>
            </Form.Group>

            <Form.Group className="d-flex justify-content-center">
              <Button variant="success" disabled={isDisabled} type="submit">
                Submit
              </Button>
            </Form.Group>
          </Form>
        </Col>
      </Row>
    </Container>
    :
    <Navigate to="/notAccessible" />
  );
}
