import { useState, useEffect } from "react";
import ProductCard from "../components/ProductCard";
import { Container, Row, Col } from "react-bootstrap";


export default function AllProduct(){
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`, {
            method: "GET",
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`,
            },
        }
      )
        .then(result => result.json())
        .then(data => {
          console.log(data)
          setProducts(data.map(products => {
            return(
                    <Col sm={12} md={6} lg={4} key={products._id}  className="mb-4">
                    <ProductCard productProps={products} />
                  </Col>               
            )
          }))
        });
    }, []);
  
    return (
      <>
      <h1 className="mt-2 mb-3 title-text text-center">All Products</h1>
      <Container>
      <Row>{products}</Row>
    </Container>
    </>
    );
  }
  