import "../App.css";

import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { NavLink, Link } from "react-router-dom";

import UserContext from "../UserContext";
import { useContext, useState } from "react";


export default function AppNavBar() {

  const {user} = useContext(UserContext);

  const [expanded, setExpanded] = useState(false);

  const toggleNavbar = () => {
    setExpanded(!expanded);
  };

  return (
    <>
      <Navbar className="appNavBar-bg-color bolder-text " variant="light" expand="md" expanded={expanded}>
        <Container>
          <Navbar.Brand as={Link} to="/">
            Smart Tech-Hub
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbar-nav" onClick={toggleNavbar} />
          <Navbar.Collapse id="navbar-nav">
          <Nav className="ml-sm-auto ms-md-auto" >
            
            {user.id === null ? (
            <>
            <Nav.Link as={NavLink} to="/" onClick={toggleNavbar}>
              Product
            </Nav.Link>

              <Nav.Link as={NavLink} to="/register" onClick={toggleNavbar}>
                Register
              </Nav.Link>
              <Nav.Link as={NavLink} to="/login" onClick={toggleNavbar}>
                Login
              </Nav.Link>
              <svg className="ms-3 design" onClick={toggleNavbar}>
                {" "}
                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
              </svg>

            </>
          ) : (
            <>
              {user.isAdmin ? (
                <>
                <NavDropdown title="Product" id="product-dropdown">
                  
                <NavDropdown.Item as={NavLink} to="/" onClick={toggleNavbar}>
                     Product
                    </NavDropdown.Item>

                    <NavDropdown.Item as={NavLink} to="/addProduct" onClick={toggleNavbar}>
                      Add Product
                    </NavDropdown.Item>
                    <NavDropdown.Item as={NavLink} to="/allProduct" onClick={toggleNavbar}>
                      Retrieve All Products
                    </NavDropdown.Item>
                  </NavDropdown>
                  <Nav.Link as={NavLink} to="/logout" onClick={toggleNavbar}>
                Logout
              </Nav.Link>
              <Nav.Link onClick={toggleNavbar}>
                {user.userName}
              </Nav.Link>
                </>
              ) : (

                <>
                    
            <Nav.Link as={NavLink} to="/" onClick={toggleNavbar}>
              Product
            </Nav.Link>
              <Nav.Link as={NavLink} to="/logout" onClick={toggleNavbar}>
                Logout
              </Nav.Link>
                <svg className="ms-3 design" onClick={toggleNavbar}>
                {" "}
                <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
              </svg>
              <Nav.Link as = {Link} to="/usersOrder" onClick={toggleNavbar}>
                {user.userName}
              </Nav.Link>
              </>
              )}
            </>
          )}
           
          </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}

