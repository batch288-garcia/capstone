const express = require("express");
const productsControllers = require("../controllers/productsControllers.js");

const auth = require("../auth.js");

const router = express.Router();

// WITHOUT PARAMS

// Route for adding products
router.post("/addProduct", auth.verify, productsControllers.addProduct);

// Route for getting all products
router.get("/allProduct", auth.verify, productsControllers.getAllProducts);

// Routes for getting all active products only
router.get("/activeProducts",  productsControllers.getAllActiveProducts);


// WITH PARAMS

// Route for getting single product
router.get("/:productId", productsControllers.getProduct);

// Route for updating a product
router.patch("/:productId/updateProduct", auth.verify, productsControllers.updateProduct);

// Router for archiving products
router.patch("/:productId/archiveProduct", auth.verify, productsControllers.archiveProduct);

// Router for activating products
router.patch("/:productId/activateProduct", auth.verify, productsControllers.activateProduct);

// Route for getting all orders
router.get("/:productId/getAllOrders", auth.verify, productsControllers.getAllOrders);

module.exports = router;