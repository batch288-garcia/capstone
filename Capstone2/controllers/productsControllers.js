const Products = require("../models/Products.js");
const Users = require("../models/Users.js");

const auth = require("../auth.js");

module.exports.addProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productName = request.body.name;

	Products.findOne({name : productName})
	.then(result => {
		if(result){
			return response.send(false);
		}else{
			let newProduct = new Products({
				imgUrl : request.body.imgUrl,
				name : request.body.name,
				description : request.body.description,
				isActive : request.body.isActive,
				price : request.body.price
			});

			if(userData.isAdmin){

				newProduct;

				newProduct.save()
				.then(save => response.send(true))
				.catch(error => response.send(false));
			}else{
				return response.send(false);	
			}
		}
	})
	.catch(error => response.send(false));

}

module.exports.getAllProducts = (request, response) => {

	const userData = auth.decode(request.headers.authorization);


	if(userData.isAdmin){

		Products.find({})
		.select(`-userOrders`)
		.then(result => response.send(result))
		.catch(error => response.send(false));

	}else{
		return response.send(false);	
	}

}

module.exports.getAllActiveProducts = (request, response) => {

	Products.find({isActive : true})
	.select(`-userOrders`)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

module.exports.getProduct = (request, response) => {

	const productId = request.params.productId;

	Products.findById(productId)
	.select(`-userOrders`)
	.then(result => response.send(result))
	.catch(error => response.send(false));

}

module.exports.updateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let updatedProduct = {
		imgUrl : request.body.imgUrl,
		name : request.body.name,
		description : request.body.description,
		price : request.body.price

	}

		if(userData.isAdmin){

			Products.findByIdAndUpdate(productId, updatedProduct)
			.then(result => response.send(true))
			.catch(error => response.send(false));

		}else{
			return response.send(false)
		}


}

module.exports.archiveProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let archivedProduct = {
		isActive : request.body.isActive
	}

	if(userData.isAdmin){

		Products.findById(productId)
		.then(result => {
			if (result.isActive === true && archivedProduct.isActive === false) {
				Products.findByIdAndUpdate(productId, archivedProduct)
				.then(result => response.send(true))
				
			}else if(result.isActive === false  && archivedProduct.isActive === true){
				return response.send(false);

			}else if(result.isActive === true  && archivedProduct.isActive === true){
				return response.send(false);

			}else if(result.isActive === false && archivedProduct.isActive === false){
				return response.send(false);
			}
			
		})
		.catch(error => response.send(false));


	}else{
		return response.send("Your not an admin, you don't have access to this route!");
	}

}

module.exports.activateProduct = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const productId = request.params.productId;

	let activatedProduct = {
		isActive : request.body.isActive
	}

	if(userData.isAdmin){

		Products.findById(productId)
		.then(result => {
			if (result.isActive === false && activatedProduct.isActive === true) {
				Products.findByIdAndUpdate(productId, activatedProduct)
				.then(result => response.send(true))
				
			}else if(result.isActive === true && activatedProduct.isActive === false){
				return response.send(false);

			}else if(result.isActive === false && activatedProduct.isActive === false){
				return response.send(false);

			}else if(result.isActive === true && activatedProduct.isActive === true){
				return response.send(false);
				}
			
		})
		.catch(error => response.send(false));

	}else{
		return response.send(false);
	}

}


module.exports.getAllOrders = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	const productId = request.params.productId;

	if(userData.isAdmin){
		Products.findById(productId)
		.select(`-_id name userOrders`)
		.then(result => {
			if (result.userOrders.length === 0) {
				return response.send("There no order to show at the moment");
			}else{
				return response.send(result);
			}
		})
		.catch(error => response.send(error));
	}else{
		return response.send(`You don't have access to this route, your not an Admin!`);
		
	}

}