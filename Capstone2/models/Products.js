const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	imgUrl : {
		type: String,
		default : "https://st4.depositphotos.com/14953852/24787/v/600/depositphotos_247872612-stock-illustration-no-image-available-icon-vector.jpg"
	},

	name : {
		type : String, 
		required : [true, `Product name is required!`]
	},

	description : {
		type : String,
		required : [true, `Product description is required!`]
	},

	price: {
	  type: Number,
	  required: [true, "Product price is required!"],
	  get: (value) => parseFloat(value).toFixed(2), // Getter to format the value when fetched from the database
	  set: (value) => parseFloat(value).toFixed(2)  // Setter to format the value when saved to the database
	},

	isActive : {
		type : Boolean,
		default : true
	},

	createdOn : {
		type : Date,
		default : new Date()

	},

	userOrders : [

					{
						userId : {
							type : String,
							required : [true, `User ID is required!`]
						},

						orderId : {
							type : String,
							required : [true, `Order ID is required!`]
						}
					}

				 ]

});

const Products = new mongoose.model("Products", productSchema);

module.exports = Products;