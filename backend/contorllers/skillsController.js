const express = require("express");
const Skills = require("../models/skills.js");

// Create Skills
module.exports.addProgrammingSkill = (request, response) => {
  const title = request.body.title;
  Skills.findOne({
    "programming.title": title,
  })
    .then((result) => {
      if (result) {
        response.send("Sorry, Duplicate record has been found.");
      } else {
        const newProgrammingSkill = new Skills({
          programming: {
            title,
            description: request.body.description,
            rating: request.body.rating,
          },
        });

        newProgrammingSkill
          .save()
          .then((save) =>
            response.send("Successfully added new Programming Skill!")
          )
          .catch((error) =>
            response.send(
              "Sorry, Unable to add new Programming Skill. Please try again." +
                error
            )
          );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

module.exports.addSoftSkill = (request, response) => {
  const skillName = request.body.skillName;
  Skills.findOne({ "softSkills.skillName": skillName })
    .then((result) => {
      if (result) {
        response.send("Sorry, Duplicate softskill found.");
      } else {
        const newSoftSkills = new Skills({
          softSkills: { skillName },
        });
        newSoftSkills
          .save()
          .then((save) => response.send("Successfully added new soft skill"))
          .catch((error) =>
            response.send("Sorry, Unable to send softskill. Please try again")
          );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

module.exports.addAddtionalSkill = (request, response) => {
  const skillName = request.body.skillName;
  Skills.findOne({ "additionalSkills.skillName": skillName })
    .then((result) => {
      if (result) {
        response.send("Sorry, Duplicate additional found.");
      } else {
        const newSoftSkills = new Skills({
          additionalSkills: { skillName },
        });
        newSoftSkills
          .save()
          .then((save) =>
            response.send("Successfully added new additional skill")
          )
          .catch((error) =>
            response.send(
              "Sorry, Unable to send additional skill. Please try again"
            )
          );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// Retrive Active programming skill
module.exports.getActiveProgrammingSKill = (request, response) => {
  Skills.find({ "programming.isActive": true })
    .then((result) => {
      if (result.length === 0) response.send("No active programming skills");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

module.exports.getActiveSoftSkill = (request, response) => {
  Skills.find({ "softSkills.isActive": true })
    .then((result) => {
      if (result.length === 0) response.send("No active programming skills");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

module.exports.getActiveAdditionalSkill = (request, response) => {
  Skills.find({ "additionalSkills.isActive": true })
    .then((result) => {
      if (result.length === 0) response.send("No Active Additional Skills");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// Retrieve deactived programming skills
module.exports.getDeactivatedProgSkills = (request, response) => {
  Skills.find({ "programming.isActive": false })
    .then((result) => {
      if (result.length === 0)
        response.send("No deactivated programming skills");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrieve Deactivated Soft Skills
module.exports.getDeactivatedSoftSkills = (request, response) => {
  Skills.find({ "softSkills.isActive": false })
    .then((result) => {
      if (result.length === 0)
        response.send("No Deactivated Soft Skills found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error:" + error));
};

// Retrieve Deactivated Additional Skills
module.exports.getDeactivatedAddSkills = (request, response) => {
  Skills.find({ "additionalSkills.isActive": false })
    .then((result) => {
      if (result.length === 0)
        response.send("No Deactivated Additional Skills found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// Update Skills
module.exports.updateProgrammingSKill = (request, response) => {
  const programmingSkillId = request.params.programmingSkillId;

  const newProgSkill = {
    programming: {
      title: request.body.title,
      description: request.body.description,
      rating: request.body.rating,
    },
  };

  Skills.findByIdAndUpdate(programmingSkillId, newProgSkill)
    .then((result) => {
      if (result) response.send("Successfully updating Programming Skill.");
      else response.send("Unable to update Programming Skill.");
    })
    .catch((error) => response.send("Catch Error: " + error));
};

module.exports.updateSoftSkill = (request, response) => {
  const softSkillId = request.params.softSkillId;

  const newSoftSkill = {
    softSkills: {
      skillName: request.body.skillName,
    },
  };

  Skills.findByIdAndUpdate(softSkillId, newSoftSkill)
    .then((result) => {
      if (result) response.send("Successfully updating Soft Skill.");
      else response.send("Unable to update soft Skill");
    })
    .catch((error) => response.send("Catch error: ") + error);
};

module.exports.updateAdditionalSkill = (request, response) => {
  const addSkillId = request.params.addSkillId;

  const newAddSkill = {
    additionalSkills: {
      skillName: request.body.skillName,
    },
  };

  Skills.findByIdAndUpdate(addSkillId, newAddSkill)
    .then((result) => {
      if (result) response.send("Successfully updating Additional Skill.");
      else response.send("Unable to send Additional Skill");
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// Delete/Archive Skills

// progskill
module.exports.archiveProgrammingSkill = (request, response) => {
  const programmingSkillId = request.params.programmingSkillId;

  const deactivateProgSkill = {
    programming: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(programmingSkillId)
    .then((result) => {
      if (
        result.programming.isActive === true &&
        deactivateProgSkill.programming.isActive === false
      ) {
        Skills.findByIdAndUpdate(programmingSkillId, deactivateProgSkill).then(
          (result) =>
            response.send("Programming Skill successfully deactivated.")
        );
      } else if (
        result.programming.isActive === false &&
        deactivateProgSkill.programming.isActive === false
      ) {
        response.send("This Programming skill is already deactivated.");
      } else
        response.send(
          "Sorry! this route is for deactivation only, cannot activate programming skill. Thanks!"
        );
    })
    .catch((error) => response.send("Catch Errro: " + error));
};

// soft skills
module.exports.archiveSoftSkill = (request, response) => {
  const softSkillId = request.params.softSkillId;

  const deactivateSoftSkill = {
    softSkills: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(softSkillId)
    .then((result) => {
      if (
        result.softSkills.isActive === true &&
        deactivateSoftSkill.softSkills.isActive === false
      ) {
        Skills.findByIdAndUpdate(softSkillId, deactivateSoftSkill).then(
          (result) => response.send("Successfully deactivating soft skill.")
        );
      } else if (
        result.softSkills.isActive === false &&
        deactivateSoftSkill.softSkills.isActive === false
      ) {
        response.send("This soft skill is already deactivated.");
      } else
        response.send(
          "Sorry, this route is for deactivation only, cannot activate soft skill. Thanks!"
        );
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// add skills
module.exports.archiveAdditionalSkill = (request, response) => {
  const addSkillId = request.params.addSkillId;

  const deactivateAddSkill = {
    additionalSkills: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(addSkillId)
    .then((result) => {
      if (
        result.additionalSkills.isActive === true &&
        deactivateAddSkill.additionalSkills.isActive === false
      ) {
        Skills.findByIdAndUpdate(addSkillId, deactivateAddSkill).then(() =>
          response.send("Additional Skill successfully deactivated.")
        );
      } else if (
        result.additionalSkills.isActive === false &&
        deactivateAddSkill.additionalSkills.isActive === false
      ) {
        response.send("This Additional Skill is already deactivated.");
      } else
        response.send(
          "Sorry this route is for deactivation only, cannot activate Additional Skill. Thanks!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Un-Archive Skills
module.exports.unArchiveProgrammingSkill = (request, response) => {
  const programmingSkillId = request.params.programmingSkillId;

  const activateProgSkill = {
    programming: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(programmingSkillId)
    .then((result) => {
      if (
        result.programming.isActive === false &&
        activateProgSkill.programming.isActive === true
      ) {
        Skills.findByIdAndUpdate(programmingSkillId, activateProgSkill).then(
          () => response.send("Programming is successfully activated.")
        );
      } else if (
        result.programming.isActive === true &&
        activateProgSkill.programming.isActive === true
      ) {
        response.send("This programming skill is already active.");
      } else
        response.send(
          "Sorry this route for activating programming skill only. cannot deactive. Thanks!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};

module.exports.unArchiveSoftSkill = (request, response) => {
  const softSkillId = request.params.softSkillId;

  const activateSoftSkill = {
    softSkills: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(softSkillId)
    .then((result) => {
      if (
        result.softSkills.isActive === false &&
        activateSoftSkill.softSkills.isActive === true
      ) {
        Skills.findByIdAndUpdate(softSkillId, activateSoftSkill).then(() =>
          response.send("Soft Skill is successfully activated.")
        );
      } else if (
        result.softSkills.isActive === true &&
        activateSoftSkill.softSkills.isActive === true
      ) {
        response.send("This soft skill is already activated.");
      } else
        response.send(
          "Srry this route is for activating soft skill only, cannot deactivate. Thanks!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};

module.exports.unArchiveAdditionalSkill = (request, response) => {
  const addSkillId = request.params.addSkillId;

  const activateAddSkill = {
    additionalSkills: {
      isActive: request.body.isActive,
    },
  };

  Skills.findById(addSkillId)
    .then((result) => {
      if (
        result.additionalSkills.isActive === false &&
        activateAddSkill.additionalSkills.isActive === true
      ) {
        Skills.findByIdAndUpdate(addSkillId, activateAddSkill).then(() =>
          response.send("Additional Skill successfully activated.")
        );
      } else if (
        result.additionalSkills.isActive === true &&
        activateAddSkill.additionalSkills.isActive === true
      ) {
        response.send("This additional skill is alreadt activated.");
      } else
        response.send(
          "Sorry this route is for activating additional skill only, cannot deactivate. Thanks!"
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};
