const Project = require("../models/projects.js");

// Create Project
module.exports.addProject = (request, response) => {
  Project.findOne({ projectTitle: request.body.projectTitle })
    .then((result) => {
      if (result) {
        return response.send("Duplicate project found.");
      } else {
        const newProject = new Project({
          cardTitle: request.body.cardTitle,
          projectTitle: request.body.projectTitle,
          description: request.body.description,
          projectImg: [
            {
              img1: request.body.img1,
              img2: request.body.img2,
              img3: request.body.img3,
              img4: request.body.img4,
              img5: request.body.img5,
              img6: request.body.img6,
            },
          ],
        });

        newProject
          .save()
          .then((save) => {
            if (save) response.send("Successfully adding new project");
            else response.send("Unable to add new project please try again.");
          })
          .catch((error) => response.send("Catch error inside else: " + error));
      }
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrieve Active Projects
module.exports.getActiveProjects = (request, response) => {
  Project.find({ isActive: true })
    .then((result) => {
      if (result.length === 0) response.send("No active project found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// Retrieve Deactivated PRoject
module.exports.getDeactivatedProjects = (request, response) => {
  Project.find({ isActive: false })
    .then((result) => {
      if (result.length === 0) response.send("No Deactivated project found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive Project
module.exports.retrieveProject = (request, response) => {
  Project.find({})
    .then((result) => {
      if (result.length === 0) response.send("No Projects found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Update Project
module.exports.updateProject = (request, response) => {
  const projectId = request.params.projectId;

  const updatedProject = {
    cardTitle: request.body.cardTitle,
    projectTitle: request.body.projectTitle,
    description: request.body.description,
    projectImg: [
      {
        img1: request.body.img1,
        img2: request.body.img2,
        img3: request.body.img3,
        img4: request.body.img4,
        img5: request.body.img5,
        img6: request.body.img6,
      },
    ],
  };

  Project.findByIdAndUpdate(projectId, updatedProject)
    .then((result) => {
      if (result) response.send("Successfully Updating project.");
      else response.send("Unable to save project please try again.");
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Delete/Archive Project
module.exports.archiveProject = (request, response) => {
  const projectId = request.params.projectId;

  const deactivateProject = {
    isActive: request.body.isActive,
  };

  Project.findByIdAndUpdate(projectId, deactivateProject)
    .then((result) => {
      if (result.isActive === true && deactivateProject.isActive === false) {
        response.send("Project successfully deactivated.");
      } else if (
        result.isActive === false &&
        deactivateProject.isActive === false
      ) {
        response.send("This project is already deactivated");
      } else
        response.send(
          "Sorry this route is for deactiation only cannot activate project."
        );
    })
    .catch((error) => response.send("Catch error: " + error));
};

// UnArchive Project
module.exports.unArchiveProject = (request, response) => {
  const projectId = request.params.projectId;

  const activateProject = {
    isActive: request.body.isActive,
  };

  Project.findByIdAndUpdate(projectId, activateProject)
    .then((result) => {
      if (result.isActive === false && activateProject.isActive === true) {
        response.send(`Project successfully activated`);
      } else if (
        result.isActive === true &&
        activateProject.isActive === true
      ) {
        response.send("This project is already activated.");
      } else
        response.send(
          "Sorry, this route is for activating project only cannot deactivate project."
        );
    })
    .catch((error) => response.send(`Catch error ${error}`));
};
