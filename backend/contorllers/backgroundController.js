const Education = require("../models/education.js");
const WorkExperience = require("../models/workexpExperience.js");

// Create Background document
module.exports.addEducation = (request, response) => {
  Education.findOne({
    school: request.body.school,
    course: request.body.course,
  })
    .then((result) => {
      if (result) {
        return response.send("Sorry, Duplicate record has been found.");
      } else {
        let newEducation = new Education({
          school: request.body.school,
          course: request.body.course,
          description: request.body.description,
          start: request.body.start,
          end: request.body.end,
          fieldOfStudy: request.body.fieldOfStudy,
        });
        newEducation
          .save()
          .then((save) =>
            response.send("Successfully added new Education to Background!")
          )
          .catch((error) =>
            response.send(
              "Sorry! Unable to add new education to background, Please try again."
            )
          );
      }
    })
    .catch((error) => response.send("Catch Error"));
};

module.exports.addWorkExperience = (request, response) => {
  WorkExperience.findOne({
    workTitle: request.body.workTitle,
  })
    .then((result) => {
      if (result) {
        return response.send("Sorry, Duplicate record has been found.");
      } else {
        let newWorkExpirience = new WorkExperience({
          company: request.body.company,
          workTitle: request.body.workTitle,
          workDuration: request.body.workDuration,
          workTask: Array.isArray(request.body.workTask)
            ? request.body.workTask.map((task) => ({ task }))
            : [{ task: request.body.workTask }],
        });
        newWorkExpirience
          .save()
          .then((save) =>
            response.send("Successfully added new WorkEpi to Background!")
          )
          .catch((error) => {
            console.error("Error saving work experience:", error);
            response.send(
              "Sorry! Unable to add new WorkEpi to background, Please try again."
            );
          });
      }
    })
    .catch((error) => {
      console.error("Error saving work experience:", error);
      response.send("Catch Error");
    });
};

// Retrive active Education
module.exports.getActiveEducation = (request, response) => {
  Education.find({ isActive: true })
    .then((result) => {
      if (result.length === 0) response.send("No active education found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive Deactive Education
module.exports.getDeactivatedEducation = (request, response) => {
  Education.find({ isActive: false })
    .then((result) => {
      if (result.length === 0) response.send("No deactived education found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive all education
module.exports.getEducation = (request, response) => {
  Education.find({})
    .then((result) => {
      if (result.length === 0) response.send("No education found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive active workeperience
module.exports.getActiveWorkExperience = (request, response) => {
  WorkExperience.find({ isActive: true })
    .then((result) => {
      if (result.length === 0) response.send("No active work expi found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrieve deactivated workexperience
module.exports.getDeactivatedWorkExperience = (request, response) => {
  WorkExperience.find({ isActive: false })
    .then((result) => {
      if (result.length === 0) response.send("No deactivated work expi found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Retrive all workeperience
module.exports.getWorkExperience = (request, response) => {
  WorkExperience.find({})
    .then((result) => {
      if (result.length === 0) response.send("No work expi found.");
      else response.send(result);
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Update Background Document
module.exports.updateEducation = (request, response) => {
  const educationId = request.params.educationId;

  let newEducation = {
    school: request.body.school,
    course: request.body.course,
    description: request.body.description,
    start: request.body.start,
    end: request.body.end,
    fieldOfStudy: request.body.fieldOfStudy,
    $inc: { __v: 1 },
  };

  Education.findByIdAndUpdate(educationId, newEducation)
    .then((result) => {
      if (result) {
        response.send("Successfully updating Education");
      } else {
        response.send("Unable to update education.");
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

module.exports.updateWorkExperience = (request, response) => {
  const workExpiId = request.params.workExpiId;

  const newWorkExpi = {
    company: request.body.company,
    workTitle: request.body.workTitle,
    workDuration: request.body.workDuration,
    workTask: request.body.workTask,
    $inc: { __v: 1 },
  };
  WorkExperience.findByIdAndUpdate(workExpiId, newWorkExpi)
    .then((result) => {
      if (!result) {
        response.send("WrkExpi not Found.");
      } else {
        response.send("Successfully updating Work Expi.");
      }
    })
    .catch((error) => response.send("Catch error: " + error));
};

// Delete/Archive Background Document
module.exports.archivedEducation = (request, response) => {
  const educationId = request.params.educationId;

  const deactivateEducation = {
    isActive: request.body.isActive,
  };

  Education.findById(educationId)
    .then((result) => {
      if (result.isActive === true && deactivateEducation.isActive === false) {
        Education.findByIdAndUpdate(educationId, deactivateEducation).then(
          (result) => response.send("Education has been updated.")
        );
      } else if (
        result.isActive === false &&
        deactivateEducation.isActive === false
      ) {
        return response.send("This education is alreadt deactivated.");
      } else {
        return response.send(
          "Sorry! this route is for deactivation only, Thanks!"
        );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

// archive Work Expi
module.exports.archiveWorkExperience = (request, response) => {
  const workExpiId = request.params.workExpiId;

  const deactivateWorkEpxi = {
    isActive: request.body.isActive,
  };

  WorkExperience.findById(workExpiId)
    .then((result) => {
      if (result.isActive === true && deactivateWorkEpxi.isActive === false) {
        WorkExperience.findByIdAndUpdate(workExpiId, deactivateWorkEpxi).then(
          (result) => response.send("Work Expi has been deactivated.")
        );
      } else if (
        result.isActive === false &&
        deactivateWorkEpxi.isActive === false
      ) {
        return response.send("Sorry, this Work Expi is already deactivated");
      } else {
        return response.send(
          "sorry this route is for deactivation only, can not activate Work Expi"
        );
      }
    })
    .catch((error) => response.send("No ducoment found: " + error));
};

// UnArchive
module.exports.unArchiveEducation = (request, response) => {
  const educationId = request.params.educationId;

  const activateEducation = {
    isActive: request.body.isActive,
  };

  Education.findById(educationId)
    .then((result) => {
      if (result.isActive === false && activateEducation.isActive === true) {
        Education.findByIdAndUpdate(educationId, activateEducation).then(
          (result) => {
            response.send("Successfully activating education");
          }
        );
      } else if (
        result.isActive === true &&
        activateEducation.isActive === true
      ) {
        response.send("Education is already activated");
      } else {
        response.send(
          "Sorry, this ropute is for acticating education only. Cannot deactivate education."
        );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};

module.exports.unArchiveWorkExperience = (request, response) => {
  const workExpiId = request.params.workExpiId;

  const activateWorkExpi = {
    isActive: request.body.isActive,
  };

  WorkExperience.findById(workExpiId)
    .then((result) => {
      if (result.isActive === false && activateWorkExpi.isActive === true) {
        WorkExperience.findByIdAndUpdate(workExpiId, activateWorkExpi).then(
          (result) => response.send("Successfuly activating Work Expi")
        );
      } else if (
        result.isActive === true &&
        activateWorkExpi.isActive === true
      ) {
        response.send("Work Expi is already activated");
      } else {
        response.send(
          "Sorry! this route is for activating Work expi only, Cannot deactivate Work Expi."
        );
      }
    })
    .catch((error) => response.send("Catch Error: " + error));
};
