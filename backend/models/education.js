const mongoose = require("mongoose");

const educationSchema = new mongoose.Schema({
  school: {
    type: String,
    required: [true, "School is required"],
  },
  course: {
    type: String,
    required: [true, "Course is required"],
  },
  description: {
    type: String,
    required: [true, "Course Description is required"],
  },
  start: {
    type: String,
    required: [true, "Course Duration is required"],
  },
  end: {
    type: String,
    required: [true, "Course Duration is required"],
  },
  fieldOfStudy: {
    type: String,
    required: [true, "Course field of study is required"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
});

const Educations = new mongoose.model("Education", educationSchema);

module.exports = Educations;
