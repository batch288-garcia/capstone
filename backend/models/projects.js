const mongoose = require("mongoose");

const projectSchema = new mongoose.Schema({
  cardTitle: {
    type: String,
    required: [true, "cardTitle is required"],
  },
  projectTitle: {
    type: String,
    required: [true, "project Title is required"],
  },
  description: {
    type: String,
    required: [true, "project descripton is reuqired"],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  projectImg: [
    {
      img1: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
      img2: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
      img3: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
      img4: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
      img5: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
      img6: {
        type: String,
        default:
          "https://t3.ftcdn.net/jpg/04/34/72/82/360_F_434728286_OWQQvAFoXZLdGHlObozsolNeuSxhpr84.jpg",
      },
    },
  ],
});

const Project = new mongoose.model("Project", projectSchema);

module.exports = Project;
